﻿using ApiMockService.Configuration;
using Xunit;

namespace UnitTest
{    
    public class TestApplicationSettings
    {
        ApplicationSettings settings;
        public TestApplicationSettings()
        {
            settings = new ApplicationSettings();
        }

        [Fact]
        public void TestApplicationsAttr_CanBeGetAndSet()
        {
            string stringToTest = "VMU:ACTIVE";

            settings.Applications = stringToTest;

            Assert.True(settings.Applications == stringToTest);
        }

        [Fact]
        public void TestConnectionStringAttr_CanBeGetAndSet()
        {
            string stringToTest = "BCP4,mipass";

            settings.ConnectionString = stringToTest;

            Assert.True(settings.ConnectionString == stringToTest);
        }

        [Fact]
        public void TestGraphApiEndpointAttr_CanBeGetAndSet()
        {
            string stringToTest = "mycool.end.point";

            settings.GraphApiEndpoint = stringToTest;

            Assert.True(settings.GraphApiEndpoint == stringToTest);
        }

        [Fact]
        public void TestTenantAttr_CanBeGetAndSet()
        {
            string stringToTest = "mytenant";

            settings.Tenant = stringToTest;

            Assert.True(settings.Tenant == stringToTest);
        }

        [Fact]
        public void TestGraphApiVersionAttr_CanBeGetAndSet()
        {
            string stringToTest = "27.35";

            settings.GraphApiVersion = stringToTest;

            Assert.True(settings.GraphApiVersion == stringToTest);
        }

        [Fact]
        public void TestAddInstanceAttr_CanBeGetAndSet()
        {
            string stringToTest = "instance";

            settings.AddInstance = stringToTest;

            Assert.True(settings.AddInstance == stringToTest);
        }

        [Fact]
        public void TestClientIdAttr_CanBeGetAndSet()
        {
            string stringToTest = "client_id";

            settings.ClientId = stringToTest;

            Assert.True(settings.ClientId == stringToTest);
        }

        [Fact]
        public void TestRedirectUriAttr_CanBeGetAndSet()
        {
            string stringToTest = "http://callbackurlverycool.co.ol";

            settings.RedirectUri = stringToTest;

            Assert.True(settings.RedirectUri == stringToTest);
        }
    }
}
