﻿using ApiMockService.Configuration;
using ApiMockService.Models;
using ApiMockService.Services;
using Microsoft.Extensions.Options;
using System;
using Xunit;

namespace ApiChatbotServiceTest.Services
{
    public class TestRequestService
    {
        private RequestService _service;

        public TestRequestService()
        {
            _service = new RequestService();
            CreateDirectoryForSaveJson();
        }

        [Fact]
        public void TestCreateRequest_ShouldCreateARquest()
        {
            Request request = GetRequestInstance();

            Request RequestCreated = _service.CreateRequest(request);

            Assert.NotNull(RequestCreated.RequestCode);
        }

        [Fact]
        public void TestGetRequest_ShouldReturnRequest()
        {
            Request request = GetRequestInstance();
            Request RequestCreated = _service.CreateRequest(request);

            Request RequestSaved = _service.GetRequest(Int32.Parse(RequestCreated.RequestCode.Replace("R-","")));

            Assert.Equal(RequestCreated.Description, RequestSaved.Description);
        }

        private Request GetRequestInstance()
        {
            Request request = new Request();
            request.Category = "my_category";
            request.Description = "My description";
            request.UserCode = "U29586";
            return request;
        }

        private void CreateDirectoryForSaveJson()
        {
            System.IO.Directory.CreateDirectory(Environment.CurrentDirectory + @"\MockRequests\");
        }

    }
}
