﻿using ApiMockService.Configuration;
using ApiMockService.Services;
using Microsoft.Extensions.Options;
using Xunit;

namespace ApiChatbotServiceTest.Services
{
    public class TestApplicationService
    {
        private ApplicationService _service;

        public TestApplicationService()
        {
            ApplicationSettings setting = new ApplicationSettings();
            setting.Applications = "ACSEL:INACTIVE,APOLO:ACTIVE,VMU:ACTIVE";
            var someOptions = Options.Create(setting);
            _service = new ApplicationService(someOptions);
        }

        [Fact]
        public void TestGetApplications_ShouldReturnApplicationsList()
        {
            Assert.Equal("ACSEL", _service.GetApplications()[0].name );
        }

    }
}
