﻿using ApiChatbotService.Models;
using ApiMockService.Interfaces;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace ApiMockService.Services
{
    public class TestADService : IActiveDirectoryService
    {
        public async Task<ADUser> GetUserDataByLogin(string login)
        {
            return new ADUser();
        }
    }
}