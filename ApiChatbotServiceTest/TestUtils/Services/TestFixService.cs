﻿using ApiMockService.Interfaces;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace ApiMockService.Services
{
    public class TestFixService : IFixService
    {
        public Task kill(string codusr)
        {
            return Task.FromResult(new HttpResponseMessage(System.Net.HttpStatusCode.OK));
        }
    }
}