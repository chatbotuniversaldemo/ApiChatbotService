﻿using ApiMockService.Interfaces;
using MockSupportService.Models;
using System.Collections.Generic;

namespace ApiMockService.Services
{
    public class TestApplicationService : IApplicationService
    {
        public List<Application> GetApplications()
        {
            List<Application> Applications = new List<Application>();
            Applications.Add( new Application( "ACSEL", "INACTIVE" ) );
            Applications.Add(new Application("APOLO", "ACTIVE"));
            Applications.Add(new Application("VMU", "ACTIVE"));
            return Applications;
        }
    }
}