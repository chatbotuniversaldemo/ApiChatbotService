﻿using ApiMockService.Interfaces;
using ApiMockService.Models;

namespace ApiMockService.Services
{
    public class TestRequestService : IRequestService
    {
        public Request GetRequest(int RequestNumber)
        {
            Request response = new Request();
            response.RequestCode = "R-"+ RequestNumber;
            return response;
        }

        public Request CreateRequest(Request request)
        {
            request.RequestCode = "R-12345";
            return request;
        }
    }
}