﻿using System.Linq;
using ApiMockService.Services;
using MockSupportService.Requests;
using Xunit;
using MockSupportService.Models;
using ApiMockService.Controllers;
using System.Collections.Generic;
using MockSupportService.Controllers;

namespace ApiMockServiceUnitTest.Controllers
{
    public class TestFixController
    {
        FixController _Controller;

        public TestFixController()
        {
            _Controller = new FixController( new TestFixService(), new TestADService() );
        }

        [Fact]
        public void Kill_WorksCorrectly()
        {
            FixCrashRequest Request = new FixCrashRequest();
            Request.Email = "U29586";

            Assert.True(_Controller.FixCrash(Request).Result.Status == "successful");
        }

        [Fact]
        public void Kill_RetuenUnexpectedErrorWhenUserCodeIsNull()
        {
            FixCrashRequest Request = new FixCrashRequest();
            Request.Email = null;

            Assert.True(_Controller.FixCrash(Request).Result.Status == "unexpected_error");
        }
    }
}
