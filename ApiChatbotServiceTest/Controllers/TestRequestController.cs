﻿using ApiMockService.Services;
using Xunit;
using ApiMockService.Controllers;
using ApiMockService.Models;

namespace ApiMockServiceUnitTest.Controllers
{
    public class TestRequestController
    {
        RequestController _Controller;

        public TestRequestController()
        {
            _Controller = new RequestController( new TestRequestService() );
        }

        [Fact]
        public void ReadJsonByRequestNumber_ReturnJsonObject()
        {
            Request request = _Controller.Get(572680);

            Assert.True(request.RequestCode == "R-572680");
        }

        [Fact]
        public void OpenRequest_RequestMustBeCreated()
        {
            Request request = new Request();
            request.Category = "my_category";
            request.Description = "My description";
            request.UserCode = "U29586";

            Request RequestCreated = _Controller.Post(request);
            
            Assert.True(RequestCreated.RequestCode == "R-12345");
        }
    }
}
