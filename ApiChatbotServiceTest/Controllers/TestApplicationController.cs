﻿using System.Linq;
using ApiMockService.Services;
using Xunit;
using MockSupportService.Models;
using ApiMockService.Controllers;
using System.Collections.Generic;

namespace ApiMockServiceUnitTest.Controllers
{
    public class TestApplicationController
    {
        ApplicationController _Controller;

        public TestApplicationController()
        {
            _Controller = new ApplicationController( new TestApplicationService() );
        }

        [Fact]
        public void ApplicationsStatus_ReturnListOfApplications()
        {
            IEnumerable<Application> Applications = _Controller.Get();
            
            Assert.True(Applications.Count() == 3);
        }
    }
}
