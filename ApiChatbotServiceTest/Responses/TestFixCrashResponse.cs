﻿using Xunit;
using MockSupportService;

namespace UnitTest.Requests
{
    public class TestFixCrashResponse
    {
        private FixCrashResponse Response;

        public TestFixCrashResponse()
        {
            this.Response = new FixCrashResponse();
        }

        [Fact]
        public void CanBeInstanciate()
        {
            Assert.True(this.Response.GetType() == typeof(FixCrashResponse) );
        }

        [Fact]
        public void StatusAttr_IsAnString()
        {
            Assert.True(this.Response.Status.GetType() == typeof(string));
        }
    }
}
