﻿using MockSupportService.Models;
using Xunit;

namespace UnitTest
{    
    public class TestApplicationModel
    {   
        [Fact]
        public void TestConstructor_SetNameAttr()
        {
            Assert.True(this.GetInstanceApplication().name == "MyName");
        }

        [Fact]
        public void TestConstructor_SetStatusAttr()
        {
            Assert.True(this.GetInstanceApplication().status == "MyStatus");
        }

        private Application GetInstanceApplication( string name = "MyName", string status = "MyStatus")
        {
            return new Application(name, status);
        }
    }
}
