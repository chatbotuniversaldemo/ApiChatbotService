﻿using MockSupportService.Requests;
using Xunit;

namespace ApiMockServiceUnitTest.Requests
{
    
    public class TestFixCrashAcselRequest
    {
        private FixCrashRequest FixCrashRequest;

        public TestFixCrashAcselRequest()
        {
            this.FixCrashRequest = new FixCrashRequest();
        }

        [Fact]
        public void TestPropertyUserCode_CanBeSet()
        {
            string UserCode = "U29586";

            this.FixCrashRequest.Email = UserCode;

            Assert.True( this.FixCrashRequest.Email == UserCode);
        }

        [Fact]
        public void TestPropertySystem_CanBeSet()
        {
            string System = "ACSEL";

            this.FixCrashRequest.System = System;

            Assert.True(this.FixCrashRequest.System == System);
        }

        [Fact]
        public void TestPropertyEmailWhenHasSIP_SIPShouldBeReplaceByEmptyString()
        {
            string Email = "mrivera@universal.com.do";
            string EmailWithSIP = "sip:"+ Email;

            this.FixCrashRequest.Email = EmailWithSIP;

            Assert.True(this.FixCrashRequest.Email == Email);
        }
    }
}
