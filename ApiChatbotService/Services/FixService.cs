﻿using ApiMockService.Configuration;
using ApiMockService.Interfaces;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Threading.Tasks;

namespace ApiMockService.Services
{
    public class FixService : IFixService
    {
        private IOptions<ApplicationSettings> _settings;
        public FixService(IOptions<ApplicationSettings> settings)
        {
            _settings = settings;
        }

        public async Task kill(string codusr)
        {
            using (var con = new OracleConnection(_settings.Value.ConnectionString))
            {
                await con.OpenAsync();
                var cmd = con.CreateCommand();
                cmd.CommandText = "uniserv.pkg_sessions.KILL_SESSION_BY_USER";
                cmd.CommandType = CommandType.StoredProcedure;

                var par = cmd.CreateParameter();
                par.Value = codusr;
                par.OracleDbType = OracleDbType.Varchar2;

                cmd.Parameters.Add(par);

                await cmd.ExecuteNonQueryAsync();
            }
        }
    }
}