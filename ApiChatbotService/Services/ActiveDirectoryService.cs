﻿using ApiMockService.Configuration;
using ApiMockService.Interfaces;
using Microsoft.Extensions.Options;
using MockSupportService.Models;
using System.Collections.Generic;
using System.DirectoryServices;
using Jayrock.Json;
using System.Net.Http;
using System.Globalization;
using System;
using System.Net.Http.Headers;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Threading.Tasks;
using UsuarioAD;
using ApiChatbotService.Models;
using Newtonsoft.Json;
using ApiChatbotService.Responses;
using System.Linq;
using Microsoft.WindowsAzure.MediaServices.Client;
using System.Security.Claims;

namespace ApiMockService.Services
{
    public class ActiveDirectoryService : IActiveDirectoryService
    {
        private HttpClient httpClient = new HttpClient();
        private IOptions<ApplicationSettings> _settings;
        private AuthenticationContext authContext = null;
        public ActiveDirectoryService(IOptions<ApplicationSettings> settings)
        {
            _settings = settings;
            string authority = String.Format(CultureInfo.InvariantCulture, _settings.Value.AddInstance, _settings.Value.Tenant);
            authContext = new AuthenticationContext(authority, true, new FileCache());
        }
        public async Task<ADUser> GetUserDataByLogin(string login)
        {
            AuthenticationResult result = await authContext.AcquireTokenAsync(_settings.Value.GraphApiEndpoint, _settings.Value.ClientId, new Uri(_settings.Value.RedirectUri), new PlatformParameters(PromptBehavior.Never));
            string graphRequest = String.Format(CultureInfo.InvariantCulture, "{0}{1}/users?api-version={2}&$filter=startswith(userPrincipalName, '{3}')", _settings.Value.GraphApiEndpoint, _settings.Value.Tenant, _settings.Value.GraphApiVersion, login);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, graphRequest);

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", result.AccessToken);
            HttpResponseMessage httpResponse = httpClient.SendAsync(request).Result;
            string stringObject = httpResponse.Content.ReadAsStringAsync()
                                               .Result
                                               .Replace("\\", "")
                                               .Trim(new char[1] { '"' });
            ADUsersResponse adUsersResponse = JsonConvert.DeserializeObject<ADUsersResponse>(stringObject);
            return adUsersResponse.value.FirstOrDefault();
        }
    }
}