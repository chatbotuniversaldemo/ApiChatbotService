﻿using ApiMockService.Configuration;
using ApiMockService.Interfaces;
using ApiMockService.Models;
using Microsoft.Extensions.Options;
using MockSupportService.Requests;
using Newtonsoft.Json;
using System;
using System.IO;

namespace ApiMockService.Services
{
    public class RequestService : IRequestService
    {
        public Request GetRequest( int RequestNumber )
        {
            using (StreamReader r = new StreamReader(Environment.CurrentDirectory + @"\MockRequests\" + RequestNumber.ToString() + ".json"))
            {
                dynamic json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<Request>(json);
            }
        }
        public Request CreateRequest(Request request)
        {
            Random rnd = new Random();
            string json = JsonConvert.SerializeObject(request);
            int idResponse = rnd.Next(999999);
            System.IO.File.WriteAllText(Environment.CurrentDirectory + @"\MockRequests\" + idResponse + ".json", json);
            request.RequestCode = "R-" + idResponse.ToString();
            return request;
        }
    }
}