﻿using ApiMockService.Configuration;
using ApiMockService.Interfaces;
using Microsoft.Extensions.Options;
using MockSupportService.Models;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace ApiMockService.Services
{
    public class ApplicationService : IApplicationService
    {
        private IOptions<ApplicationSettings> _settings;
        public ApplicationService(IOptions<ApplicationSettings> settings)
        {
            _settings = settings;
        }

        public List<Application> GetApplications()
        {
            string[] aplicationsConfig = _settings.Value.Applications.Split(',');
            List<Application> Applications = new List<Application>();
            foreach (string aplicationConfigString in aplicationsConfig)
            {
                string[] aplicationConfigs = aplicationConfigString.Split(':');
                Applications.Add(new Application(aplicationConfigs[0], aplicationConfigs[1]));
            }
            return Applications;
        }
    }
}