﻿
namespace MockSupportService.Requests
{
    public class FixCrashRequest
    {
        private string email;
        public string Email
        {
            get
            {
                return this.email;
            }
            set
            {
                this.email = value != null ? value.Replace("sip:","") : null;
            }
        }
        public string System { get; set; }
    }
}