﻿
namespace MockSupportService.Requests
{
    public class GetUserDataByLoginRequest
    {
        public string Login { get; set; }
    }
}