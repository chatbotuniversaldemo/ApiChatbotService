﻿using MockSupportService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMockService.Configuration
{
    public class ApplicationSettings
    {
        public string Applications { get; set; }
        public string ConnectionString { get; set; }
        public string GraphApiEndpoint { get; set; }
        public string Tenant { get; set; }
        public string GraphApiVersion { get; set; }
        public string AddInstance { get; set; }
        public string ClientId { get; set; }
        public string RedirectUri { get; set; }
    }
}
