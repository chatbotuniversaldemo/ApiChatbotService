﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MockSupportService.Models
{
    public class Application
    {
        public string name { get; set; }
        public string status { get; set; }

        public Application( string name, string status )
        {
            this.name = name;
            this.status = status; 
        }
    }
}