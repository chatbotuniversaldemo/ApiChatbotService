﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiMockService.Models
{
    public class Request
    {
        public string UserCode { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string RequestCode { get; set; }
        public Request()
        {
            this.Category = "";
            this.RequestCode = "";
            this.UserCode = "";
            this.Description = "";
        }
    }
}