﻿using ApiMockService.Interfaces;
using Microsoft.AspNetCore.Mvc;
using MockSupportService.Models;
using System.Collections.Generic;

namespace ApiMockService.Controllers
{
    [Route("api/[controller]")]
    public class ApplicationController : Controller
    {
        IApplicationService _Service;

        public ApplicationController(IApplicationService ApplicationService)
        {
            _Service = ApplicationService;
        }

        [HttpGet]
        public IEnumerable<Application> Get()
        {
            return _Service.GetApplications();
        }
    }
}
