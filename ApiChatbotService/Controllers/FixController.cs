﻿using ApiChatbotService.Models;
using ApiMockService.Interfaces;
using Jayrock.Json;
using Microsoft.AspNetCore.Mvc;
using MockSupportService.Requests;
using System;
using System.Threading.Tasks;

namespace MockSupportService.Controllers
{
    public class FixController : Controller
    {
        IFixService _Service;
        IActiveDirectoryService _ADService;

        public FixController(IFixService Service, IActiveDirectoryService ActiveDirectoryService)
        {
            _Service = Service;
            _ADService = ActiveDirectoryService;
        }

        [Route("api/fix")]
        [HttpPost]
        public async Task<FixCrashResponse> FixCrash([FromBody] FixCrashRequest request)
        {
            FixCrashResponse response = new FixCrashResponse();
            try
            {
                response.Status = "unexpected_error";
                if (request.Email != null)
                {
                    ADUser user = await _ADService.GetUserDataByLogin(request.Email);
                    await this._Service.kill(user.state);
                    response.Status = "successful";
                }
            } catch (Exception e)
            {
                response.Status = e.Message;
            }
            return response;
        }
    }
}
