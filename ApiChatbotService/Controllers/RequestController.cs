﻿using ApiMockService.Interfaces;
using ApiMockService.Models;
using Microsoft.AspNetCore.Mvc;

namespace ApiMockService.Controllers
{
    [Route("api/[controller]")]
    public class RequestController : Controller
    {
        IRequestService _Service;

        public RequestController(IRequestService Service)
        {
            _Service = Service;
        }

        [HttpPost]
        public Request Post([FromBody] Request request)
        {
            return _Service.CreateRequest(request);
        }

        [HttpGet("{code}")]
        public Request Get(int code)
        {
            return _Service.GetRequest(code);
        }
    }
}