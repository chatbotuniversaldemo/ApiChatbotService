﻿using ApiChatbotService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiChatbotService.Responses
{
    public class ADUsersResponse
    {
        public List<ADUser> value { get; set; }
    }
}
