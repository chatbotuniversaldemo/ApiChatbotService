﻿using MockSupportService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiMockService.Interfaces
{
    public interface IApplicationService
    {
        List<Application> GetApplications();
    }
}
