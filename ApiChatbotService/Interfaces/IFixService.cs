﻿using System.Threading.Tasks;

namespace ApiMockService.Interfaces
{
    public interface IFixService
    {
        Task kill(string email);
    }
}
