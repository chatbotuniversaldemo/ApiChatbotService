﻿using ApiChatbotService.Models;
using Jayrock.Json;
using System.Threading.Tasks;

namespace ApiMockService.Interfaces
{
    public interface IActiveDirectoryService
    {
        Task<ADUser> GetUserDataByLogin( string login );
    }
}
