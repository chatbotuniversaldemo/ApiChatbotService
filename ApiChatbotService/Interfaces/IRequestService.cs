﻿using ApiMockService.Models;
using MockSupportService.Models;
using MockSupportService.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiMockService.Interfaces
{
    public interface IRequestService
    {
        Request GetRequest(int RequestNumber);
        Request CreateRequest(Request request);
    }
}